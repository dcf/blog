title: Tails 2.4 is out
---
pub_date: 2016-06-07
---
author: tails
---
tags:

tails
anonymous operating system
tails releases
---
categories:

partners
releases
---
_html_body:

<p>This release fixes <a href="https://tails.boum.org/security/Numerous_security_holes_in_2.3/" rel="nofollow">many security issues</a> and users should upgrade as soon as possible.</p>

<h2>New features</h2>

<ul>
<li>We enabled the automatic account configuration of <em>Icedove</em> which discovers the correct parameters to connect to your email provider based on your email address. We improved it to rely only on secure protocol and we are working on sharing these improvements with Mozilla so that users of <em>Thunderbird</em> outside Tails can benefit from them as well.
</li>
</ul>

<h2>Upgrades and changes</h2>

<ul>
<li>Update <em>Tor Browser</em> to <a href="https://blog.torproject.org/blog/tor-browser-601-released" rel="nofollow">6.0.1</a>, based on Firefox 45.</li>
<li>Remove the preconfigured <span class="geshifilter"><code class="php geshifilter-php"><span style="color: #666666; font-style: italic;">#tails</span></code></span> IRC channel. <a href="https://tails.boum.org/support/chat/" rel="nofollow">Join us on XMPP instead!</a></li>
<li>Always display minimize and maximize buttons in titlebars. (<a href="https://labs.riseup.net/code/issues/11270" rel="nofollow">#11270</a>)</li>
<li>Remove <em>GNOME Tweak Tool</em> and <em>hledger</em>. You can add them back using the <a href="https://tails.boum.org/doc/first_steps/persistence/configure/#additional_software" rel="nofollow"><em>Additional software packages</em></a> persistence feature.</li>
<li>Use secure HKPS OpenPGP key server in <em>Enigmail</em>.</li>
<li>Harden our firewall by rejecting <span class="geshifilter"><code class="php geshifilter-php">RELATED</code></span> packets and restricting Tor to only send <span class="geshifilter"><code class="php geshifilter-php"><span style="color: #000000; font-weight: bold;">NEW</span></code></span> TCP <span class="geshifilter"><code class="php geshifilter-php">syn</code></span> packets. (<a href="https://labs.riseup.net/code/issues/11391" rel="nofollow">#11391</a>)</li>
<li>Harden our kernel by:
<ul>
<li>Setting various security-related kernel options: <span class="geshifilter"><code class="php geshifilter-php">slab_nomerge slub_debug<span style="color: #339933;">=</span>FZ mce<span style="color: #339933;">=</span><span style="color: #cc66cc;">0</span> vsyscall<span style="color: #339933;">=</span>none</code></span>. (<a href="https://labs.riseup.net/code/issues/11143" rel="nofollow">#11143</a>)</li>
<li>Removing the .map files of the kernel. (<a href="https://labs.riseup.net/code/issues/10951" rel="nofollow">#10951</a>)</li>
</ul>
</li>
</ul>

<h2>Fixed problems</h2>

<ul>
<li>Update the <em>DRM</em> and <em>Mesa</em> graphical libraries. This should fix recent problems with starting Tails on some hardware. (<a href="https://labs.riseup.net/code/issues/11303" rel="nofollow">#11303</a>)</li>
<li>Some printers that stopped working in Tails 2.0 should work again. (<a href="https://labs.riseup.net/code/issues/10965" rel="nofollow">#10965</a>)</li>
<li>Enable Packetization Layer Path MTU Discovery for IPv4. This should make the connections to <span class="geshifilter"><code class="php geshifilter-php">obfs4</code></span> Tor bridges more reliable. (<a href="https://labs.riseup.net/code/issues/9268" rel="nofollow">#9268</a>)</li>
<li>Remove our custom ciphers and MACs settings for SSH. This should fix connectivity issues with other distributions such as OpenBSD. (<a href="https://labs.riseup.net/code/issues/%237315" rel="nofollow">##7315</a>)</li>
<li>Fix the translations of <em>Tails Upgrader</em>. (<a href="https://labs.riseup.net/code/issues/10221" rel="nofollow">#10221</a>)</li>
<li>Fix displaying the details of a circuit in <em>Onion Circuits</em> when using Tor bridges. (<a href="https://labs.riseup.net/code/issues/11195" rel="nofollow">#11195</a>)</li>
</ul>

<p>For more details, read our <a href="https://git-tails.immerda.ch/tails/plain/debian/changelog" rel="nofollow">changelog</a>.</p>

<h2>Known issues
<ul>
<li>The automatic account configuration of <em>Icedove</em> freezes when connecting to some email providers. (<a href="https://labs.riseup.net/code/issues/11486" rel="nofollow">#11486</a>)</li>
<li>In some cases sending an email with Icedove results in the error: "The message could not be sent using Outgoing server (SMTP) mail.riseup.net for an unknown reason." When this happens, simply click "Ok" and try again and it should work. (<a href="https://labs.riseup.net/code/issues/10933" rel="nofollow">#10933</a>)</li>
<li>The update of the <em>Mesa</em> graphical library introduce new problems at least on AMD HD 7770 and nVidia GT 930M.</li>
</ul>
</h2>

<p>See the list of <a href="https://tails.boum.org/support/known_issues/" rel="nofollow">long-standing issues</a>.</p>

<h2>Get Tails 2.4
<ul>
<li>To install, follow our <a href="https://tails.boum.org/install/" rel="nofollow">installation instructions</a>.</li>
<li>To upgrade, an automatic upgrade is available from 2.3 to 2.4.
<p>If you cannot do an automatic upgrade or if you fail to start after an automatic upgrade, please try to do a <a href="https://tails.boum.org/upgrade/" rel="nofollow">manual upgrade</a>.</p></li>
<li><a href="https://tails.boum.org/install/download/" rel="nofollow">Download Tails 2.4.</a></li>
</ul>
<h2>What's coming up?
</h2></h2>

<p>Tails 2.5 is <a href="https://tails.boum.org/contribute/calendar/" rel="nofollow">scheduled</a> for August 2.</p>

<p>Have a look at our <a href="https://labs.riseup.net/code/projects/tails/roadmap" rel="nofollow">roadmap</a> to see where we are heading to.</p>

<p>We need your help and there are many ways to <a href="https://tails.boum.org/contribute/" rel="nofollow">contribute to Tails</a> (<a href="https://tails.boum.org/contribute/how/donate/" rel="nofollow">donating</a> is only one of them). Come <a href="https://tails.boum.org/about/contact/#tails-dev" rel="nofollow">talk to us</a>!</p>

<h2>Support and feedback</h2>

<p>For support and feedback, visit the <a href="https://tails.boum.org/support/" rel="nofollow">Support section</a> on the Tails website.


</p>

