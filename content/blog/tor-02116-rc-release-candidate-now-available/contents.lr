title: Tor 0.2.1.16-rc Release Candidate now available
---
pub_date: 2009-06-24
---
author: phobos
---
tags:

release candidate
bug fixes
security fixes
hidden service fixes
---
categories:

onion services
releases
---
_html_body:

<p>Tor 0.2.1.16-rc speeds up performance for fast exit relays, and fixes<br />
a bunch of minor bugs.</p>

<p><a href="https://www.torproject.org/download" rel="nofollow">https://www.torproject.org/download</a></p>

<p>Changes in version 0.2.1.16-rc - 2009-06-20<br />
<strong>Security fixes:</strong></p>

<ul>
<li>Fix an edge case where a malicious exit relay could convince a<br />
      controller that the client's DNS question resolves to an internal IP<br />
      address. Bug found and fixed by "optimist"; bugfix on 0.1.2.8-beta.</li>
</ul>

<p><strong>Major performance improvements (on 0.2.0.x):</strong></p>

<ul>
<li>Disable and refactor some debugging checks that forced a linear scan<br />
      over the whole server-side DNS cache. These accounted for over 50%<br />
      of CPU time on a relatively busy exit node's gprof profile. Found<br />
      by Jacob.</li>
<li>Disable some debugging checks that appeared in exit node profile<br />
      data.</li>
</ul>

<p><strong>Minor features:</strong></p>

<ul>
<li>Update to the "June 3 2009" ip-to-country file.</li>
<li>Do not have tor-resolve automatically refuse all .onion addresses;<br />
      if AutomapHostsOnResolve is set in your torrc, this will work fine.</li>
</ul>

<p><strong>Minor bugfixes (on 0.2.0.x):</strong></p>

<ul>
<li>Log correct error messages for DNS-related network errors on<br />
      Windows.</li>
<li>Fix a race condition that could cause crashes or memory corruption<br />
      when running as a server with a controller listening for log<br />
      messages.</li>
<li>Avoid crashing when we have a policy specified in a DirPolicy or<br />
      SocksPolicy or ReachableAddresses option with ports set on it,<br />
      and we re-load the policy. May fix bug 996.</li>
<li>Hidden service clients didn't use a cached service descriptor that<br />
      was older than 15 minutes, but wouldn't fetch a new one either,<br />
      because there was already one in the cache. Now, fetch a v2<br />
      descriptor unless the same descriptor was added to the cache within<br />
      the last 15 minutes. Fixes bug 997; reported by Marcus Griep.</li>
</ul>

<p><strong>Minor bugfixes (on 0.2.1.x):</strong></p>

<ul>
<li>Don't warn users about low port and hibernation mix when they<br />
      provide a *ListenAddress directive to fix that. Bugfix on<br />
      0.2.1.15-rc.</li>
<li>When switching back and forth between bridge mode, do not start<br />
      gathering GeoIP data until two hours have passed.</li>
<li>Do not complain that the user has requested an excluded node as<br />
      an exit when the node is not really an exit. This could happen<br />
      because the circuit was for testing, or an introduction point.<br />
      Fix for bug 984.</li>
</ul>

<p>The original announcement can be found at <a href="http://archives.seul.org/or/talk/Jun-2009/msg00288.html" rel="nofollow">http://archives.seul.org/or/talk/Jun-2009/msg00288.html</a></p>

---
_comments:

<a id="comment-1634"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1634" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>PETER (not verified)</span> said:</p>
      <p class="date-time">June 24, 2009</p>
    </div>
    <a href="#comment-1634">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1634" class="permalink" rel="bookmark">thanx my friend 4 ur great</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>thanx my friend 4 ur great effort. Tor rocks! Keep on going guys.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-1638"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1638" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 25, 2009</p>
    </div>
    <a href="#comment-1638">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1638" class="permalink" rel="bookmark">Surefire way to increase TOR speed by an order of magnitude</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>All that is necessary to build a powerful TOR network with an order of magnitude speed increase is the following:</p>
<p>1) Make install of TOR much simpler.  Look at the latest ergonomic studies.</p>
<p>2) Prioritize TOR users who are also relays.  The biggest complaint about TOR is the speed.  (lack of)  If you guaranteed users a certain speed priority in USING TOR if they registered as a Relay, then you would give TOR users the "financial incentive" to become TOR relays.</p>
<p>That's all you need to do.  Sit back and trust in the power of social networking and self interest to do the rest.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-1645"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1645" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">June 26, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-1638" class="permalink" rel="bookmark">Surefire way to increase TOR speed by an order of magnitude</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-1645">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1645" class="permalink" rel="bookmark">Re: Surefire way to increase Tor speed</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Quite so. The only little problem is the huge anonymity problems that are introduced by the idea.</p>
<p>See my in-depth analysis here:<br />
<a href="https://blog.torproject.org/blog/two-incentive-designs-tor" rel="nofollow">https://blog.torproject.org/blog/two-incentive-designs-tor</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-1655"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1655" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Blake (not verified)</span> said:</p>
      <p class="date-time">June 28, 2009</p>
    </div>
    <a href="#comment-1655">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1655" class="permalink" rel="bookmark">Performance Increase</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><a href="http://213.251.185.41/tor-upgrade.png" rel="nofollow">Here's the speedup</a> when moving from .15-rc to .16-rc (at 0200).</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-1743"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1743" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 03, 2009</p>
    </div>
    <a href="#comment-1743">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1743" class="permalink" rel="bookmark">Error in Vidalia while shutting down Windows Vista</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Is there a place where one can submitt bugs?<br />
Have not found one, so I discribe it here:</p>
<p>I got an error every time I want to shut down Windows Vista (with SP2).<br />
The Error is:<br />
"Vidalia Kontroll-Panel"<br />
"Die Anweisung in 0x0045048f verweist auf Speicher 0x00000018. Der Vargang read konnte nicht im Speicher durchgefuehrt werden."<br />
Have to click on OK to shut down Vista.</p>
<p>Got the error with:<br />
- Tor 0.2.0.33 Vidalia 0.1.10<br />
- Tor 0.2.0.35 Vidalia 0.1.14<br />
- Tor 0.2.1.16 Vidalia 0.1.14</p>
<p>Any solution?</p>
<p>Regards - Frank</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-1753"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1753" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">July 05, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-1743" class="permalink" rel="bookmark">Error in Vidalia while shutting down Windows Vista</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-1753">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1753" class="permalink" rel="bookmark">Re: Error in Vidalia while shutting down Windows Vista</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>There is a place to report Vidalia bugs. See the "Support" section<br />
on the Vidalia web page: <a href="https://www.torproject.org/vidalia/#Support" rel="nofollow">https://www.torproject.org/vidalia/#Support</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
