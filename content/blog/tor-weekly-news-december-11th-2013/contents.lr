title: Tor Weekly News — December 11th, 2013
---
pub_date: 2013-12-11
---
author: lunar
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the twenty-fourth issue of Tor Weekly News, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">weekly newsletter</a> that covers what is happening in the Tor community.</p>

<h1>Introducing a new Lead Automation Engineer</h1>

<p>The Tor Project welcomed a new member, Nicolas Vigier, in the role of Lead Automation Engineer. He swiftly got down to business by <a href="https://lists.torproject.org/pipermail/tor-dev/2013-December/005906.html" rel="nofollow">putting out a call</a> for automation-related feedback “from developers of any tor components”, promising a summary of “the current status regarding build, packaging, testing, and what should be done” to improve the automated processes involved in Tor development, as well as “some proposals and a general plan for the work to be done during the coming months”, an <a href="https://people.torproject.org/~boklm/automation/tor-automation-review.html" rel="nofollow">early version of which is already online</a>. A warm welcome to him!</p>

<h1>Freedom of the Press Foundation launch a support campaign for Tor</h1>

<p>In his <a href="https://lists.torproject.org/pipermail/tor-reports/2013-December/000396.html" rel="nofollow">report for November</a>, Roger Dingledine wrote: “We've also been pondering lately how to do a fundraising or donations drive, to help move us off our reliance on government funders.” The good news is that Tor has many supporters.</p>

<p>The Freedom of the Press Foundation just started <a href="https://pressfreedomfoundation.org/blog/2013/12/freedom-press-foundation-launches-campaign-support-encryption-tools-journalists" rel="nofollow">a campaign to support encryption tools for journalists</a> that is going to last for two months. The campaign is gathering funds for Tor core development work, the <a href="https://tails.boum.org/" rel="nofollow">Tails live system</a>, the encrypted mobile communication tools <a href="https://whispersystems.org/" rel="nofollow">RedPhone and TextSecure</a>, and the encrypted email platform <a href="https://leap.se/" rel="nofollow">LEAP</a>.</p>

<p><a href="https://pressfreedomfoundation.org/" rel="nofollow">Spread the word</a> and help make the campaign a success. Direct <a href="https://www.torproject.org/donate/" rel="nofollow">donations to The Tor Project</a> are also always possible.</p>

<h1>More monthly status reports for November 2013</h1>

<p>The wave of regular monthly reports from Tor project members for the month of November continued, with reports from <a href="https://lists.torproject.org/pipermail/tor-reports/2013-December/000396.html" rel="nofollow">Roger Dingledine</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2013-December/000397.html" rel="nofollow">Georg Koppen</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2013-December/000398.html" rel="nofollow">Karsten Loesing</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2013-December/000399.html" rel="nofollow">Matt Pagan</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2013-December/000400.html" rel="nofollow">Nicolas Vigier</a> <a href="https://lists.torproject.org/pipermail/tor-reports/2013-December/000401.html" rel="nofollow">Philipp Winter</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2013-December/000402.html" rel="nofollow">Damian Johnson</a>, and <a href="https://lists.torproject.org/pipermail/tor-reports/2013-December/000403.html" rel="nofollow">Noel David Torres Taño </a>.</p>

<h1>Miscellaneous news</h1>

<p>David Fifield has produced <a href="https://blog.torproject.org/blog/pluggable-transports-bundles-2418-rc-1-pt1-and-2418-rc-2-pt1-firefox-17011esr" rel="nofollow">updated “pluggable transport bundles”</a> based on the 2.4.18 browser bundles. Be sure to upgrade!</p>

<p>David also <a href="https://lists.torproject.org/pipermail/tor-dev/2013-December/005913.html" rel="nofollow">announced</a> great progress in integrating our current set of pluggable transports within the new Tor Browser 3 build infrastructure.</p>

<p>Thanks to Himanshu from India, Andrew Lewman was able to <a href="https://lists.torproject.org/pipermail/tor-mirrors/2013-December/000394.html" rel="nofollow">deploy a new script</a> to list <a href="https://www.torproject.org/getinvolved/mirrors.html.en" rel="nofollow">mirrors of the Tor Project's website</a>. The new code will ensure that mirrored files are “100% the same as those on torproject.org (hashing, pgp signatures, etc)” and will remove and re-instate mirrors accordingly.</p>

<p>Thanks <a href="https://lists.torproject.org/pipermail/tor-mirrors/2013-October/000381.html" rel="nofollow">BarkerJr</a>, <a href="https://lists.torproject.org/pipermail/tor-mirrors/2013-November/000384.html" rel="nofollow">DevRandom</a>, and <a href="https://lists.torproject.org/pipermail/tor-mirrors/2013-November/000389.html" rel="nofollow">Userzap</a> for setting up new mirrors!</p>

<p>Nathan Freitas <a href="https://lists.torproject.org/pipermail/tor-talk/2013-December/031331.html" rel="nofollow">announced that Orfox was going to replace Orweb</a> in the Guardian Project set of secure communication tools. Orfox is based on the Firefox engine and is likely to behave more like the Tor Browser. “However, Orfox is still very early (not alpha yet) and has not been fully tested. I would either do your own testing and report back to us, or only use the app for non sensitive/critical browsing for now”, Nathan <a href="https://lists.torproject.org/pipermail/tor-talk/2013-December/031340.html" rel="nofollow">added</a>.</p>

<p>arkmd <a href="https://lists.torproject.org/pipermail/tor-dev/2013-December/005901.html" rel="nofollow">reported</a> that TorBirdy was saving unencrypted drafts on the remote server on their system. Sukhbir Singh has <a href="https://bugs.torproject.org/10309#comment:4" rel="nofollow">not been able to reproduce the issue so far</a> but he is thinking of <a href="https://lists.torproject.org/pipermail/tor-dev/2013-December/005903.html" rel="nofollow">disabling the automatic save feature entirely</a>.</p>

<p>Christian <a href="https://lists.torproject.org/pipermail/tor-talk/2013-December/031353.html" rel="nofollow">announced</a> that Globe is “now officially hosted on the torproject servers”. Credits to Karsten Loesing and Peter Palfrader for setting up the infrastructure. Have a look at this <a href="https://globe.torproject.org/" rel="nofollow">new Tor relay and bridge explorer</a>!</p>

<p>ghostmaker <a href="https://lists.torproject.org/pipermail/tor-dev/2013-December/005908.html" rel="nofollow">advertised</a> “a small new tool for Windows called InjectSOCKS that can force other Windows software to do TCP connections via SOCKS.” InjectSOCKS source code and binaries are available from <a href="http://sourceforge.net/projects/injectsocks" rel="nofollow">the project page</a>.</p>

<p>The Tails team has issued a <a href="https://tails.boum.org/news/test_incremental_upgrades/" rel="nofollow">call for testing regarding incremental upgrades</a> — one more step on the path to providing Tails users with easy upgrades.</p>

<p>The <a href="https://mailman.boum.org/pipermail/tails-dev/2013-December/004405.html" rel="nofollow">release schedule for Tails 0.22.1</a> has been published by intrigeri. The expected release date for this point release is January 21th.</p>

<h1>Tor help desk roundup</h1>

<p>Multiple users have asked the help desk specifically for assistance with IPv6 bridges. Currently getting IPv6 bridges is not too easy, but the <a href="https://bugs.torproject.org/9127" rel="nofollow">issue is known</a> and should be solved as the bridge distributor is improved. IPv6 bridges are functional in (at least part of) China, but we do not have many of them to distribute. Please set up an IPv6 bridge if you can: it's only <a href="https://people.torproject.org/~linus/ipv6-relay-howto.html" rel="nofollow">one “ORPort” line to add</a> to the <a href="https://www.torproject.org/docs/bridges.html.en#RunningABridge" rel="nofollow">usual configuration</a>.</p>

<p>This issue of Tor Weekly News has been assembled by Lunar, harmony, dope457, Matt Pagan, Roger Dingledine and Karsten Loesing.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

