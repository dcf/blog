title: What's new in Tor 0.2.9.8?
---
pub_date: 2016-12-19
---
author: dgoulet
---
tags:

hidden services
onion services
shared randomness
---
categories: onion services
---
_html_body:

<p>Today, we've released the first stable version of the 0.2.9.x series, bringing exciting new features to Tor. The series has seen 1406 commits from 32 different contributors. Please, see the <a href="https://gitweb.torproject.org/tor.git/tree/ChangeLog?h=release-0.2.9&amp;id=tor-0.2.9.8#n1" rel="nofollow">ChangeLog</a> for more details about what has been done.</p>

<p>This post will outline three features (among many other things) that we are quite proud of and want to describe in more detail.</p>

<h2>Single Onion Service</h2>

<p>Over the past several years, we've collaborated with many large scale service providers such as Facebook and Riseup, organizations that deployed <a href="https://www.torproject.org/docs/tor-hidden-service.html.en" rel="nofollow">Onion Services</a> to improve their performance.</p>

<p>Onion services are great because they offer both anonymity on the service <b>and</b> the client side. However, there are cases where the onion service does not require anonymity. The main example of this is when the service provider does not need to hide the location of its servers.</p>

<p>As a reminder to the reader, an onion service connection between a client and a service goes through 6 hops, while a regular connection with Tor is 3 hops. Onion services are much slower than regular Tor connections because of this.</p>

<p>Today, we are introducing Single Onion Services! With this new feature, a service can now specify in its configuration file that it does not need anonymity, thus cutting the 3 hops between the service and its Rendezvous Point and speeding up the connection.</p>

<p><img width="530" /></p>

<p>For security reasons, if this option is enabled, <b>only</b> single onion service can be configured. They can't coexist with a regular onion service. Because this removes the anonymity aspect of the service, we took extra precautions so that it's very difficult to enable a single onion by mistake. In your torrc file, here is how you do it:</p>

<blockquote><p>
HiddenServiceNonAnonymousMode 1<br />
HiddenServiceSingleHopMode 1
</p></blockquote>

<p>Please read about these options before you enable them in the <a href="https://www.torproject.org/docs/tor-manual.html.en" rel="nofollow">manual page</a>.</p>

<h2>Shared Randomness</h2>

<p>We've talked about <a href="https://blog.torproject.org/blog/mission-montreal-building-next-generation-onion-services" rel="nofollow">this</a> before but now it is a reality. At midnight UTC every day, directory authorities collectively generate a <b>global random value</b> that cannot be predicted in advance. This daily fresh random value is the foundation of our next generation onion service work coming soon to a Tor near you.</p>

<p>In the consensus file, they will look like this; if all goes well, at 00:00 UTC, consensus will have a new one:</p>

<blockquote><p>
shared-rand-current-value Hq+hGlzwAVetJ2zkO70riH/SEMNri+c7Ps8xERZ3a0o=<br />
shared-rand-previous-value CY5TncVAltDpkBKZUBYT1canvqmVoNuweiKVZIilHfs=
</p></blockquote>

<p>Thanks to atagar, the <a href="https://stem.torproject.org" rel="nofollow"></a>Stem Library version 1.5.0 supports parsing the shared random values from the consensus. See <a href="https://stem.torproject.org/api/descriptor/networkstatus.html#stem.descriptor.networkstatus.NetworkStatusDocumentV3" rel="nofollow">here</a> for more information!</p>

<p>Voluntarily, we haven't exposed those values to the control port yet and will wait for a full stable release cycle in order to make sure it's stable enough for a third party application to use them (<a href="https://trac.torproject.org/19925" rel="nofollow">https://trac.torproject.org/19925</a>).</p>

<h2>Mandatory ntor handshake</h2>

<p>This is another important security feature introduced in the new release. Authorities, relays and clients now require <a href="https://gitweb.torproject.org/torspec.git/tree/proposals/216-ntor-handshake.txt" rel="nofollow">ntor</a> keys in all descriptors, for all hops, for all circuits, and for all other roles.</p>

<p>In other words, except for onion services (and this will be addressed with the next generation), only ntor is used--now finally dropping the <a href="https://gitweb.torproject.org/torspec.git/tree/tor-spec.txt#n844" rel="nofollow">TAP</a> handshake.</p>

<p>This results in better security for the overall network and users.</p>

<p>Enjoy this new release!</p>

---
_comments:

<a id="comment-226379"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-226379" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 19, 2016</p>
    </div>
    <a href="#comment-226379">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-226379" class="permalink" rel="bookmark">Does a client user know, a</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Does a client user know, a priori, whether a given .Onion URI is one hop or three?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-226444"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-226444" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">December 19, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-226379" class="permalink" rel="bookmark">Does a client user know, a</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-226444">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-226444" class="permalink" rel="bookmark">No (unless they do something</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>No (unless they do something esoteric like trying to track round-trip timing to guess how many hops are involved).</p>
<p>In the glorious future, when we're using next-generation onion services, the new onion descriptors will have a field where the onion service can say "I'm being a single onion service", and then clients will be able to predict it:<br />
<a href="https://trac.torproject.org/projects/tor/ticket/19642" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/19642</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-226704"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-226704" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 20, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to arma</p>
    <a href="#comment-226704">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-226704" class="permalink" rel="bookmark">You write, &quot;where an onion</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You write, "where an onion service CAN say..." (emphasis added)  </p>
<p>This field should be forced to accuracy and uneditable by the service. I remain disenchanted with the idea that a client can swap between three hops and six hops with the client ignorant of when such swapping occurs and how it may impact their anonymity.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-226907"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-226907" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">December 21, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-226704" class="permalink" rel="bookmark">You write, &quot;where an onion</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-226907">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-226907" class="permalink" rel="bookmark">You, the Tor client visiting</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You, the Tor client visiting the onion service, still make your own three-hop circuit, which is where your anonymity comes from.</p>
<p>So in the case where you want protection from the onion service, you can't rely on the 3 hops it chooses anyway, because it could be choosing them to hurt you.</p>
<p>You're right that for the case where you want protection from some external attacker, it's not totally clear that just your three hops provides less or the same or more privacy than the full six hops. But I think for most attacks, if your three hops are good then you're good, and if your three hops are not good then you're not good. I would welcome somebody coming up with an attack where the distinction matters in practice. Thanks!</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div><a id="comment-226380"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-226380" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 19, 2016</p>
    </div>
    <a href="#comment-226380">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-226380" class="permalink" rel="bookmark">Tor Browser not</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tor Browser not signed<br />
<a href="https://objective-see.com/products/whatsyoursign.html" rel="nofollow">https://objective-see.com/products/whatsyoursign.html</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-227590"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-227590" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 22, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-226380" class="permalink" rel="bookmark">Tor Browser not</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-227590">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-227590" class="permalink" rel="bookmark">&quot;Malicious activity has been</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"Malicious activity has been detected from your computer or another computer on your network.</p>
<p>"Your computer may be compromised with a virus and part of a botnet, sending spam or attacking websites. We recommend for you to update your anti-virus software and perform a full scan.</p>
<p>"Complete the challenge below to be granted temporary access to this website."</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-226468"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-226468" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 19, 2016</p>
    </div>
    <a href="#comment-226468">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-226468" class="permalink" rel="bookmark">Great! Now I can do upgrades</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Great! Now I can do upgrades from apt-transport-tor without worrying too much about speed! :)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-226540"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-226540" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 20, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-226468" class="permalink" rel="bookmark">Great! Now I can do upgrades</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-226540">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-226540" class="permalink" rel="bookmark">Not yet, next-gen onion</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Not yet, next-gen onion services aren't available yet!</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-226475"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-226475" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 20, 2016</p>
    </div>
    <a href="#comment-226475">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-226475" class="permalink" rel="bookmark">How could one de-anonymize</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>How could one de-anonymize this non-anonymous onion service?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-226535"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-226535" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 20, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-226475" class="permalink" rel="bookmark">How could one de-anonymize</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-226535">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-226535" class="permalink" rel="bookmark">By compromising the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>By compromising the rendezvous server, I guess. The onion service connects directly to it.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-226574"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-226574" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 20, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-226535" class="permalink" rel="bookmark">By compromising the</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-226574">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-226574" class="permalink" rel="bookmark">How can the rendezvous tell</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>How can the rendezvous tell if it's an onion service or just some random folk using Tor?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-226594"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-226594" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  pastly
  </article>
    <div class="comment-header">
      <p class="comment__submitted">pastly said:</p>
      <p class="date-time">December 20, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-226574" class="permalink" rel="bookmark">How can the rendezvous tell</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-226594">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-226594" class="permalink" rel="bookmark">I think the &quot;attack&quot; would</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I think the "attack" would go like this. (I put attack in quotes because the onion service operator should be very very aware of the fact this is possible and doesn't mind.)</p>
<p>- User runs a relay<br />
- User knows that foobar.onion is a single onion service<br />
- User builds a circuit to foobar.onion, using his relay as the rendezvous point</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-227420"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-227420" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 22, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to pastly</p>
    <a href="#comment-227420">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-227420" class="permalink" rel="bookmark">&gt; - User builds a circuit to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; - User builds a circuit to foobar.onion, using his relay as the rendezvous point</p>
<p>I thought the rendezvous point was chosen by the Tor node hosting the hidden service, not the one connecting to it? Or am I confusing that with the introduction point? Of course, the hidden service could by chance choose a "malicious" user's relay as the rendezvous point, but I don't understand how the user could make that happen on demand.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-227896"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-227896" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 23, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-227420" class="permalink" rel="bookmark">&gt; - User builds a circuit to</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-227896">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-227896" class="permalink" rel="bookmark">Yeah, that&#039;s what I thought</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yeah, that's what I thought too!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-228069"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-228069" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">December 23, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-227420" class="permalink" rel="bookmark">&gt; - User builds a circuit to</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-228069">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-228069" class="permalink" rel="bookmark">The service chooses the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The service chooses the intro points, and the client chooses the rendezvous point.</p>
<p>For the final circuit (the one the actual data goes over, aka the rendezvous circuit), it's:</p>
<p>User &lt;-&gt; User's Guard &lt;-&gt; User-picked Middle &lt;-&gt; User-picked rendezvous point &lt;-&gt; Service-picked middle &lt;-&gt; Service-picked middle &lt;-&gt; Service's Guard &lt;-&gt; Service.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div></div></div><a id="comment-226613"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-226613" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 20, 2016</p>
    </div>
    <a href="#comment-226613">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-226613" class="permalink" rel="bookmark">So, in other  words  this</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>So, in other  words  this option is for the users that use Tor for normal browsing and not for complicated stuff. This is what I understand. Part me if I'm wrong...and if I'm right i need to ask when this function is going to be integrated in the Onion Browser made by  Mike Tidas because from what I readed from the other post it looks like Mike Tidas who works at ProPublica  is in collaboration with you  for providing the Tor browser FOR IOS.<br />
A question I have is why you don't make an official Tor browser or at least put some work in the existent Onion browser we already have ( trying to perfecting it)? There are many options in the app store but because this apps  are not made by Tor project it very hard to trust them. This is why I and probably other users want something original.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-226774"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-226774" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 20, 2016</p>
    </div>
    <a href="#comment-226774">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-226774" class="permalink" rel="bookmark">Tor Browser&#039;s Tor circuit</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tor Browser's Tor circuit box can't distinguish between hidden service and single hop onion service.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-226818"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-226818" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 20, 2016</p>
    </div>
    <a href="#comment-226818">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-226818" class="permalink" rel="bookmark">Which of the services listed</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Which of the services listed in onion.torproject.org (<a href="http://yz7lpwfhhzcdyc5y.onion/" rel="nofollow">http://yz7lpwfhhzcdyc5y.onion/</a>) are single hop services?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-226905"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-226905" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">December 21, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-226818" class="permalink" rel="bookmark">Which of the services listed</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-226905">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-226905" class="permalink" rel="bookmark">Great question! I think the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Great question! I think the answer right now is "none". But we could fix that.</p>
<p>I've opened <a href="https://trac.torproject.org/projects/tor/ticket/21049" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/21049</a> to see if anybody else finds it interesting.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-226891"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-226891" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 21, 2016</p>
    </div>
    <a href="#comment-226891">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-226891" class="permalink" rel="bookmark">I stopped by the OFTC #tor</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I stopped by the OFTC #tor IRC chatroom on behalf of the Retroshare project which has a great many users now running Retroshare hidden nodes via tor, to ask about the new tor shared randomness project to see if that is something the Retroshare developers would be interested in adding a more random base for advanced entropy pools on encryption.</p>
<p>The #tor chat ops didn't know anything about the project and began pushing if off on a 'game' then began challenging me to post the link as they apparently didn't believe it existed. I just remember scanning it by reading some early Tor blog entrys months earlier and it took me a day to locate it and return to the OFTC IRC #tor chatroom and post the link, explaining the project in the #tor chatroom.</p>
<p>There was no response, later before leaving a user asked for help which I provided when there was no one stepping up to provide the answers and solution to their problem.</p>
<p>I returned the next day and found I was gagged by the ops, prevented from posting, prevented from any comments whatsoever and remained so months later to present.</p>
<p>This for 'asking about information regarding the Tor Shared Randomness project in the 'Tor' chatroom. </p>
<p>I own 5 chatrooms in 5 different IRC servers including the #retroshare OFTC chatroom, if anyone wishes to address this issue and behavior to me, you can find me there or any of a dozen Retroshare Chat lobbys, channels and forums where I have helped and advised hundreds of users that have wanted to setup their applications including Retroshare routed through the tor router.</p>
<p>I also wrote the tor guide for Retroshare users found on multiple websites which have helped countless new users globally become introduced to tor and able to use the Retroshare communications global network via tor.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-226999"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-226999" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 21, 2016</p>
    </div>
    <a href="#comment-226999">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-226999" class="permalink" rel="bookmark">Shared randomness: awesome!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Shared randomness: awesome!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-228997"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-228997" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 27, 2016</p>
    </div>
    <a href="#comment-228997">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-228997" class="permalink" rel="bookmark">Why not to use</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Why not to use cryptocurrencies as randomness sources?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-229028"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-229028" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">December 27, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-228997" class="permalink" rel="bookmark">Why not to use</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-229028">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-229028" class="permalink" rel="bookmark">A) There are potentially</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>A) There are potentially many new dependencies which are brought in, in terms of engineering complexity, with a design like that.</p>
<p>B) Systems like Bitcoin are not immune to attack to influence the next block, and new attacks continue to show up. So the security analysis becomes quite complex. Also, since our security goals would not be the same as those of the foocoin design, they might end up changing something that hurts us but doesn't hurt them.</p>
<p>C) The security of a particular cryptocurrency is a function of its popularity. So even if bitcoin is popular this year, who knows about three or four years from now? If suddenly bitcoin is boring and foocoin is exciting, and people leave bitcoin, we're left with a cryptocurrency that's surprisingly more vulnerable than we expected.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-229729"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-229729" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 02, 2017</p>
    </div>
    <a href="#comment-229729">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-229729" class="permalink" rel="bookmark">There is a lively discussion</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>There is a lively discussion of the single hop "feature" here:</p>
<p><a href="https://lists.torproject.org/pipermail/tor-talk/2016-December/042748.html" rel="nofollow">https://lists.torproject.org/pipermail/tor-talk/2016-December/042748.ht…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
