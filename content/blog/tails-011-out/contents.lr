title: Tails 0.11 is out!
---
pub_date: 2012-05-07
---
author: tails
---
tags:

tails
anonymous operating system
---
categories: partners
---
_html_body:

<p>This new 0.11 release is an important milestone in Tails history.</p>

<p><a href="https://tails.boum.org/download/" rel="nofollow">Download it now.</a></p>

<p><strong>Changes</strong></p>

<p>Notable user-visible changes include:</p>

<ul>
<li><strong>Tails Greeter</strong>, the login screen which obsoletes the language selection boot menu. Tails Greeter also adds some new options:
<ul>
<li>Activating persistence (see below).</li>
<li>Setting a sudo password. Unlike earlier Tails releases, full sudo access via an empty password is not available any more. In fact, full sudo access is disabled per default, but can be enabled by setting this password. See <a href="https://tails.boum.org/doc/first_steps/startup_options/index.en.html#tails_greeter" rel="nofollow">the documentation</a> for details.</li>
</ul>
</li>
<li><strong>Tails USB installer.</strong> This graphical user interface mostly obsoletes our old instructions of cat'ing the .iso directly onto a block device. All of the USB drive must be dedicated to Tails; a bit of extra space is reserved so that future Tails releases will fit when upgrading, and the rest can be used for persistence (see below) or manually formatted if the user so wishes. See <a href="https://tails.boum.org/doc/first_steps/usb_installation/index.en.html" rel="nofollow">the USB installation documentation</a> for details.</li>
<li><strong>Persistence</strong> can optionally be used when running Tails from a USB drive. Application configurations and arbitrary directories can be made persistent. See <a href="https://tails.boum.org/doc/first_steps/persistence/index.en.html" rel="nofollow">the persistence documentation</a> for details.</li>
<li><strong>iceweasel</strong>
<ul>
<li>Install iceweasel 10.0.4esr-1 (Extended Support Release).</li>
<li>Search plugins: replace Debian-provided DuckDuckGo search plugin with the "HTML SSL" one; add ixquick.com; remove Scroogle.</li>
<li>Enable TLS false start.</li>
</ul>
</li>
<li><strong>Vidalia</strong>: upgrade to 0.2.17-1+tails1</li>
<li><strong>Internationalization</strong>
<ul>
<li>Install all available iceweasel l10n packages.</li>
<li>Add fonts for Hebrew, Thai, Khmer, Lao and Korean languages.</li>
<li>Add bidi support.</li>
<li>Don't purge locales anymore.</li>
<li>Don't remove any Scribus translations anymore.</li>
</ul>
</li>
<li><strong>Hardware support</strong>
<ul>
<li>Linux 3.2.15-1 (linux-image-3.2.0-2-amd64).</li>
<li>Fix low sound level on MacBook5,2.</li>
<li>Disable laptop-mode-tools automatic modules. This modules set often needs some amount of hardware-specific tweaking to work properly. This makes them rather not well suited for a Live system.</li>
</ul>
</li>
<li><strong>Software</strong>
<ul>
<li>Install GNOME keyring.</li>
<li>Install the Traverso multitrack audio recorder and editor.</li>
</ul>
</li>
<li><strong>Miscellaneous</strong>
<ul>
<li>NetworkManager is now started at PostLogin time by tails-greeter, rather than as a system service.</li>
<li>Pidgin: don't use the OFTC hidden service anymore. It proved to be quite unreliable, being sometimes down for days.</li>
<li>Do not display storage volumes on Desktop. This workarounds a usability issue when persistence is enabled, and paves the way towards GNOME3's empty Desktop.</li>
<li>Don't build hybrid ISO images anymore. They boot less reliably on a variety of hardware, and are made less useful by us shipping a USB installer from now on.</li>
</ul>
</li>
</ul>

<p>Plus the usual bunch of minor bug reports and improvements.</p>

<p>See the <a href="http://git.immerda.ch/?p=amnesia.git;a=blob_plain;f=debian/changelog;hb=refs/tags/0.11" rel="nofollow">online Changelog</a> for technical details.</p>

<p>Don't hesitate to <a href="https://tails.boum.org/support/index.en.html" rel="nofollow">get in touch with us</a>.</p>

