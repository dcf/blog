title: Tor Weekly News — February 12th, 2014
---
pub_date: 2014-02-12
---
author: lunar
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the sixth issue of Tor Weekly News in 2014, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">weekly newsletter</a> that covers what is happening in the Tor community.</p>

<h1>Tails 0.22.1 is out</h1>

<p>The Tails team cut its 36th release on February 4th. Their Debian-based live operating system continues to provide anonymity by ensuring that all outgoing connections are routed through Tor, and privacy by ensuring that no traces are left without the user’s knowledge.</p>

<p><a href="https://tails.boum.org/news/version_0.22.1/" rel="nofollow">Tails 0.22.1</a> contains <a href="https://tails.boum.org/security/Numerous_security_holes_in_0.22/" rel="nofollow">security fixes</a> to Firefox, NSS, and Pidgin. It also brings an updated Linux kernel and several fixes for regressions and small issues.</p>

<p>While advertised as a minor version, the new incremental upgrades are a major usability improvement. Previously, upgrading Tails basically meant installing Tails again by downloading the image and putting it on a DVD or a USB stick. Users who store persistent data in their Tails instance then had to use this new medium to upgrade the stick with their data. A tedious process, to say the least. Now, with incremental upgrades, Tails users with USB sticks will be prompted to perform a few clicks, wait, and reboot to get their system up-to-date.</p>

<p>One usability change might surprise long time Tails users: the browser now has to be manually opened when Tor has successfully reached the network.</p>

<p>As always, be sure to <a href="https://tails.boum.org/doc/first_steps/upgrade/" rel="nofollow">upgrade</a>! Users of Tails 0.22 on USB sticks can do so easily by running the <em>Tails Upgrader</em> application in the <em>Tails</em> menu.</p>

<h1>Tor Browser Bundle 3.5.2 is released</h1>

<p>The Tor Browser team delivers a <a href="https://blog.torproject.org/blog/tor-browser-352-released" rel="nofollow">new Tor Browser Bundle</a>. Version 3.5.2 brings Tor users <a href="https://www.mozilla.org/security/known-vulnerabilities/firefoxESR.html#firefox24.3" rel="nofollow">important security fixes from Firefox</a> and contains fixes to the “new identity” feature, window size rounding, and the welcome screen with right-to-left language, among others.</p>

<p>The curious can take a peek at the <a href="https://gitweb.torproject.org/builders/tor-browser-bundle.git/blob/a1bab4013e:/Bundle-Data/Docs/ChangeLog.txt" rel="nofollow">changelog</a> for more details. Every Tor user is encouraged to upgrade as soon possible. Jump to the <a href="https://www.torproject.org/download/download-easy.html" rel="nofollow">download page</a>!</p>

<h1>Call to bridge operators to deploy ScrambleSuit</h1>

<p>In the beginning there was Tor. When censors started filtering every known relay address, <a href="https://gitweb.torproject.org/torspec.git/blob_plain/HEAD:/proposals/125-bridges.txt" rel="nofollow">bridges</a> were invented as a way to access the Tor network through unlisted relays. Deep packet inspection systems then started to filter Tor based on its traffic signature, so <a href="https://gitweb.torproject.org/torspec.git/blob_plain/HEAD:/pt-spec.txt" rel="nofollow">pluggable transports</a> and obfucation protocols were designed in order to prevent bridge detection.</p>

<p>Currently, obfuscation is achieved through “obfs2” and “obfs3”. obfs2 is flawed; it’s detectable by deep packet inspection and is being phased out. obfs3 is unfortunately still vulnerable to active probing attacks. As obfs3 bridges are open to anyone, an attacker who uses a traffic classifier and finds an unclassified connection can figure out if it’s Tor simply by trying to connect through the same destination.</p>

<p><a href="http://www.cs.kau.se/philwint/scramblesuit/" rel="nofollow">ScrambleSuit</a> comes to the rescue. On top of making the traffic harder to recognize by timing or volume characteristics, ScrambleSuit requires a shared secret between the bridge and the client. A censor looking at the connection won’t have this secret, and therefore be unable to connect to the bridge and confirm that it’s Tor.</p>

<p>obfsproxy 0.2.6 was <a href="https://gitweb.torproject.org/pluggable-transports/obfsproxy.git/commit/a3b43d475c4172" rel="nofollow">released last week</a> and adds ScrambleSuit to the set of available pluggable transports. Bridge operators are now <a href="https://lists.torproject.org/pipermail/tor-relays/2014-February/003886.html" rel="nofollow">called</a> to update their software and configuration. At least Tor 0.2.5.1-alpha is required. The latest version of obfsproxy can be installed from <a href="https://gitweb.torproject.org/pluggable-transports/obfsproxy.git" rel="nofollow">source</a>, <a href="https://pypi.python.org/pypi/obfsproxy" rel="nofollow">pip</a> and <a href="https://lists.torproject.org/pipermail/tor-relays/2014-February/003894.html" rel="nofollow">Debian unstable</a>.</p>

<p>There must be a critical mass of bridges before ScrambleSuit is made available to the Tor users who need it, so please help!</p>

<h1>More status reports for January 2014</h1>

<p>The wave of regular monthly reports from Tor project members for the month of January continued. <a href="https://lists.torproject.org/pipermail/tor-reports/2014-February/000446.html" rel="nofollow">Kevin P Dyer</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-February/000447.html" rel="nofollow">Nick Mathewson</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-February/000448.html" rel="nofollow">Georg Koppen</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-February/000449.html" rel="nofollow">Karsten Loesing</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-February/000450.html" rel="nofollow">Jacob Appelbaum</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-February/000451.html" rel="nofollow">Arturo Filastò</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-February/000452.html" rel="nofollow">Isis Lovecruft</a> and <a href="https://lists.torproject.org/pipermail/tor-reports/2014-February/000453.html" rel="nofollow">Nicolas Vigier</a> all released their reports this week.</p>

<p>Roger Dingledine has also sent the <a href="https://lists.torproject.org/pipermail/tor-reports/2014-February/000454.html" rel="nofollow">report</a> to SponsorF.</p>

<h1>Miscellaneous news</h1>

<p>Most Tor developers will gather next week in Reykjavík, Iceland for the <a href="https://trac.torproject.org/projects/tor/wiki/org/meetings/2014WinterDevMeeting" rel="nofollow">2014 winter meeting</a>. Expect a drop in activity on the usual communication channels while everyone is busy with face-to-face conversations. See <a href="https://blog.torproject.org/events/tors-winter-2014-developers-meeting-reykjavik-iceland" rel="nofollow">upcoming events</a> for activities open to the larger Tor community.</p>

<p>David Fifield is <a href="https://lists.torproject.org/pipermail/tor-qa/2014-February/000324.html" rel="nofollow">looking for testers</a> for experimental 3.5.2 browser bundles with tor-fw-helper. “tor-fw-helper is a tool that uses UPnP or NAT-PMP to forward a port automatically” — something that <a href="https://crypto.stanford.edu/flashproxy/" rel="nofollow">flashproxy</a> requires. David is “interested in finding out how likely it is to work”.</p>

<p>David Goulet gave us an <a href="https://lists.torproject.org/pipermail/tor-dev/2014-February/006172.html" rel="nofollow">update on the development of Torsocks 2.x</a>. He hopes to perform a “full on release” after the Tor developers meeting.</p>

<p>”The Trying Trusted Tor Traceroutes project is coming closer to the next data review (03/2014)” <a href="https://lists.torproject.org/pipermail/tor-relays/2014-February/003865.html" rel="nofollow">wrote</a> Sebastian Urbach. If you are a relay operator, please help find out how Tor performs against network-level attackers. The team now has a <a href="http://datarepo.cs.illinois.edu/relay_scoreboard.html" rel="nofollow">scoreboard</a> with feedback for the participants.</p>

<p>One relay started to act funny regarding its advertised bandwidth. Roger Dingledine quickly <a href="https://lists.torproject.org/pipermail/tor-talk/2014-February/032094.html" rel="nofollow">reported his worries</a> to the tor-talk mailing list. A couple of hours later Hyoung-Kee Choi <a href="https://lists.torproject.org/pipermail/tor-talk/2014-February/032096.html" rel="nofollow">accounted</a> that one of the students from his research group had made a mistake while experimenting on the Tor bandwidth scanner. Directory authorities are now restricting its usage in the consensus.</p>

<p>On February 11th, the Tor Project participated on <a href="https://thedaywefightback.org/" rel="nofollow">The Day We Fight Back</a>, a global day of mobilization against NSA mass surveillance.</p>

<h1>Tor help desk roundup</h1>

<p>Tor supporters are often curious about the legal risks involved in running a Tor relay. The Tor Project is not aware of any country where running Tor is a punishable offense. Running a bridge relay or a non-exit relay is the best way to grow the Tor network without being exposed to additional legal scrutiny. The decision to run an exit relay should be made only after carefully reviewing the <a href="https://blog.torproject.org/running-exit-node" rel="nofollow">best practices</a>. Unlike non-exit and bridge operators, exit relay operators need to be prepared to respond to abuse complaints.</p>

<p>Users continue to express interest in a 64-bit Tor Browser Bundle for Windows. Work to provide this new variant is <a href="https://bugs.torproject.org/10026" rel="nofollow">on-going</a>.</p>

<h1>News from Tor StackExchange</h1>

<p>strugee is running a Fast, Running and Valid relay and wonders <a href="https://tor.stackexchange.com/q/1485/88" rel="nofollow">when the relay will get the V2Dir flag</a>. weasel answered that relays should “get the V2Dir flag simply by publishing a DirPort”, but that Tor will not always publish a DirPort: the full list can be found in the <a href="https://gitweb.torproject.org/tor.git/blob/tor-0.2.4.20:/src/or/router.c#l1018" rel="nofollow">source code</a>.</p>

<p>Ivar noted that the site <a href="https://www.howsmyssl.com/" rel="nofollow">How’s my SSL</a> thinks that the SSL configuration of the Tor Browser is bad and wondered <a href="https://tor.stackexchange.com/q/1455/88" rel="nofollow">how the situation could be improved</a>. Jens Kubieziel explained some settings for about:config and pointed to a <a href="http://kubieziel.de/blog/archives/1564-Using-SSL-securely-in-your-browser.html" rel="nofollow">more detailed blog post</a>. Sam Whited also pointed out some settings for Firefox and noted that Firefox 27 <a href="https://blog.samwhited.com/2014/01/fixing-tls-in-firefox/" rel="nofollow">improved the rating to “probably good”</a> which will help the Tor Browser in the future.</p>

<p>fred set up a relay on a Windows machine where µTorrent is used besides Tor.  When Tor is enabled many trackers become unreachable, but come back as soon as the relay is disabled. An <a href="https://tor.stackexchange.com/q/1243/88" rel="nofollow">explanation to this behaviour</a> has yet to be found, don’t hesitate to chime in.</p>

<p>This issue of Tor Weekly News has been assembled by Lunar, Matt Pagan, Paul Feitzinger, qbi, Roger Dingledine and Karsten Loesing.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to<br />
get involved!</p>

