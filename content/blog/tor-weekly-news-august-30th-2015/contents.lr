title: Tor Weekly News — August 30th, 2015
---
pub_date: 2015-08-30
---
author: harmony
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the thirty-third issue in 2015 of Tor Weekly News, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">weekly newsletter</a> that covers what’s happening in the Tor community.</p>

<h1>Hash visualizations to protect against onion phishing</h1>

<p>Unlike URLs on the non-private web, the .onion addresses used by Tor hidden services are not handed out by any central authority — instead, they are derived by the hidden services themselves based on their cryptographic key information. This means that they are typically quite hard for humans to remember, unless the hidden service operator — whether by chance or by making repeated attempts — hits upon a memorable string, as in the case of <a href="https://lists.torproject.org/pipermail/tor-talk/2014-October/035413.html" rel="nofollow">Facebook’s hidden service</a>.</p>

<p>“The problem”, writes George Kadianakis, is that due to these user-unfriendly strings, “many people don’t verify the whole onion address, they just trust the onion link or verify the first few characters. This is bad since an attacker can create a hidden service with a similar onion address very easily”, then trick users into visiting that address instead for a variety of malicious purposes. This species of attack that has already been seen <a href="https://lists.torproject.org/pipermail/tor-talk/2015-June/038295.html" rel="nofollow">in the wild</a>. After discussions with other researchers in this area, George drew up a <a href="https://lists.torproject.org/pipermail/tor-dev/2015-August/009302.html" rel="nofollow">proposal</a> to incorporate visual information into the verification process: “So when TBB connects to a hidden service, it uses the onion address to generate a randomart or key poem and makes them available for the user to examine.”</p>

<p>As with all new development proposals, however, there are many unanswered questions. What kind of visualization would work best? Should there also be an auditory component, like a randomly-generated tune? How should the feature be made available to users without confusing those who have no idea what it is or why it’s needed? In short, “Some real UX research needs to be done here, before we decide something terrible.”</p>

<p>If you have clear and constructive feedback to offer on this unusual but important proposal, please send it to the tor-dev mailing list.</p>

<h1>Tor-enabled Debian mirrors</h1>

<p>Richard Hartmann, Peter Palfrader, and Jonathan McDowell have set up the first <a href="http://richardhartmann.de/blog/posts/2015/08/24-Tor-enabled_Debian_mirror/" rel="nofollow">official onion service mirrors</a> of the Debian operating system’s software package infrastructure. This means that it is now possible to update your Debian system without the update information or downloaded packages leaving the Tor network at all, preventing a network adversary from discovering information about your system. A <a href="http://richardhartmann.de/blog/posts/2015/08/25-Tor-enabled_Debian_mirror_part_2/" rel="nofollow">follow-up post</a> by Richard includes guidance on using <a href="https://retout.co.uk/blog/2014/07/21/apt-transport-tor" rel="nofollow">apt-transport-tor</a> with the new mirrors.</p>

<p>These services are only the first in what should hopefully become a fully Tor-enabled system mirroring “the complete package lifecycle, package information, and the website”. “This service is not redundant, it uses a key which is stored on the local drive, the .onion will change, and things are expected to break”, wrote Richard, but if you are interested in trying out the new infrastructure, see the write-ups for further information.</p>

<h1>Miscellaneous news</h1>

<p>David Fifield <a href="https://lists.torproject.org/pipermail/tor-dev/2015-August/009365.html" rel="nofollow">announced</a> that his 17-minute PETS talk on the theory and practice of “domain fronting”, which is the basis for Tor’s innovative and successful <a href="https://trac.torproject.org/projects/tor/wiki/doc/meek" rel="nofollow">meek pluggable transport</a>, is now available to view online.</p>

<p>Arturo Filastò <a href="https://lists.torproject.org/pipermail/tor-talk/2015-August/038822.html" rel="nofollow">announced</a> that registration for <a href="https://ooni.torproject.org/event/adina15/" rel="nofollow">ADINA15</a>, the upcoming OONI hackathon at the Italian Parliament in Rome, is now open. If you’re interested in hacking on internet censorship data in this rarified location, with the possibility of “interesting prizes” for the winning teams, see Arturo’s mail for the full details.</p>

<p>Arturo also sent out the OONI team’s <a href="https://lists.torproject.org/pipermail/tor-reports/2015-August/000900.html" rel="nofollow">July status report</a>, while Tor Summer of Privacy progress updates were submitted by <a href="https://lists.torproject.org/pipermail/tor-reports/2015-August/000897.html" rel="nofollow">Israel Leiva</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2015-August/000898.html" rel="nofollow">Cristobal Leiva</a>, and <a href="https://lists.torproject.org/pipermail/tor-dev/2015-August/009324.html" rel="nofollow">Jesse Victors</a>.</p>

<p>Fabio Pietrosanti issued an <a href="https://lists.torproject.org/pipermail/tor-talk/2015-August/038835.html" rel="nofollow">open call</a> for developers interested in working on <a href="https://globaleaks.org/" rel="nofollow">GlobaLeaks</a>, the open-source anonymous whistleblowing software. “Are you interested in making the world a better place by putting your development skills to use in a globally used free software project? Do you feel passionate about using web technologies for developing highly usable web applications?” If so, please see Fabio’s message for more information.</p>

<h1>News from Tor StackExchange</h1>

<p>saurav <a href="https://tor.stackexchange.com/q/3756/88" rel="nofollow">created a network using the Shadow simulator</a> and started with 40 guard and 40 exit nodes. After a simulation was performed, another 40/40 nodes were added.  saurav then noticed that the more recent nodes had a higher probability of being selected. Can you explain why this is the case? The users of Tor’s Q&amp;A page will be happy to know.</p>

<p>This issue of Tor Weekly News has been assembled by qbi, Lunar, nicoo, and Harmony.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

