title: Protecting your Internet traffic in volatile times
---
pub_date: 2011-02-02
---
author: phobos
---
tags:

low bandwidth clients
egypt
volatile internet
tor on satellite
tor camouflage
---
categories:

global south
network
---
_html_body:

<p>We're glad that the Internet Service Providers in Egypt are announcing their routes to the world and have <a href="http://www.renesys.com/blog/2011/02/egypt-returns-to-the-internet.shtml" rel="nofollow">rejoined the Internet</a>.  We are concerned because it is possible that traffic crossing the Egyptian border is being recorded and possibly saved for future use.  <a href="https://www.torproject.org/download/download#warning" rel="nofollow">Correctly using Tor</a> to and from Egyptian destinations will keep your traffic anonymous.</p>

<p>Tor separates who you are from where you are going on the Internet.  It's up to you, the user, to choose where you share your personal information. Currently, we do not try very hard to hide that you are using Tor.  In the past few years, we haven't needed to hide. Tor looks like an SSL connection on the wire. Your local ISP, if they are very clever, could map your current connections from your assigned IP address to the list of public Tor relays. This would only tell them you are using Tor, not where you are going on the Internet. We do offer <a href="https://www.torproject.org/docs/bridges.html.en" rel="nofollow">bridge relays</a>, which are semi-public relays published in a few selective ways. By using bridges, your ISP is unlikely to figure out you are using Tor. We need thousands more bridges, please <a href="https://www.torproject.org/docs/tor-doc-relay" rel="nofollow">join the Tor network</a> to help others.</p>

<p>Many years ago, we theorized this arms race could happen.  Recent events have turned theory into reality. We are working on improvements to make it much more difficult to detect Tor usage. These methods include <a href="https://gitweb.torproject.org/tor.git/blob/HEAD:/doc/spec/proposals/176-revising-handshake.txt" rel="nofollow">normalization of our TLS usage</a> and <a href="https://gitweb.torproject.org/tor.git/blob/HEAD:/doc/spec/proposals/ideas/xxx-pluggable-transport.txt" rel="nofollow">tunneling</a> Tor through other protocols, such as XMPP, HTTPS, and HTTP.</p>

<p>Thanks to some <a href="https://secure.avaaz.org/en/egypt_blackout/" rel="nofollow">funding from Avaaz</a>, we've also begun experimenting with ways to make Tor perform better on satellite and mesh networks.  We have Tor working well on <a href="https://guardianproject.info/apps/orbot/" rel="nofollow">mobile phone 3G and 4G connections</a>. Tor over <a href="https://secure.wikimedia.org/wikipedia/en/wiki/Very_small_aperture_terminal" rel="nofollow">VSAT</a> and <a href="https://secure.wikimedia.org/wikipedia/en/wiki/Broadband_Global_Area_Network" rel="nofollow">BGAN</a> connections does work, however it needs more research into how to better handle the variable latencies and varying available bandwidth on such connections.  The improvements that result from this research will benefit those with little to no Internet access, whether due to political unrest, natural disasters, or remote locations, who nonetheless seek to keep their online activities safe.</p>

---
_comments:

<a id="comment-9180"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-9180" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 04, 2011</p>
    </div>
    <a href="#comment-9180">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-9180" class="permalink" rel="bookmark">Looking at the Torstatus it</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Looking at the Torstatus it seems ~90% of the Relays are very slow ones. Pretty useless for guard and maybe even as middleman.</p>
<p>Would be much better if they run as bridges especially when they are on dynamic IP Adresses.</p>
<p>How about making more Advertisment to run Bridges incase the supporter has a dynamic DSL line?</p>
</div>
  </div>
</article>
<!-- Comment END -->
