title: New Release: Tails 3.9
---
pub_date: 2018-09-05
---
author: tails
---
tags:

anonymous operating system
tails
tails releases
---
categories:

partners
releases
---
_html_body:

<p>Tails 3.9 is the biggest update of Tails this year!<br />
It includes two new features on which we have been working for more than a year:</p>

<ul>
<li><em>Additional Software</em></li>
<li><em>VeraCrypt</em> integration</li>
</ul>

<p>This release also fixes <a href="https://tails.boum.org/security/Numerous_security_holes_in_3.8/" rel="nofollow">many security issues</a> and users should upgrade as soon as possible.</p>

<h1>Changes</h1>

<h2>New features</h2>

<h3>Additional Software</h3>

<p>You can now install additional software automatically when starting Tails.<br />
When installing an additional Debian package from Tails, you can decide to install it automatically every time:<br />
To check your list of additional software packages, choose <strong>Applications</strong> ▸ <strong>System Tool</strong> ▸ <strong>Additional Software</strong>.</p>

<p>The packages included in Tails are carefully tested for security. Installing additional packages might break the security built in Tails, so be careful with what you install.<br />
<a href="https://tails.boum.org/doc/first_steps/additional_software/" rel="nofollow">Read our documentation on installing additional software.</a></p>

<h3>VeraCrypt integration</h3>

<p>To unlock <em>VeraCrypt</em> volume in Tails, choose <strong>Applications</strong> ▸ <strong>System Tool</strong> ▸ <strong>Unlock VeraCrypt Volumes</strong>.<br />
The integration of <em>VeraCrypt</em> in the <em>Files</em> and <em>Disks</em> utilities was done upstream in GNOME and will be available outside of Tails in Debian 10 (Buster) and Ubuntu 18.10 (Cosmic Cuttlefish).<br />
<a href="https://tails.boum.org/doc/encryption_and_privacy/veracrypt/" rel="nofollow">Read our documentation on using <em>VeraCrypt</em> volumes.</a></p>

<h3>News reading in <em>Thunderbird</em></h3>

<p><em>Thunderbird</em> is now the official RSS and Atom news feed reader in Tails.<br />
<em>Liferea</em> will be removed from Tails in version 3.12, early 2019.</p>

<h2>Upgrades and changes</h2>

<ul>
<li>Improve the configuration of the persistent storage to make it easier to scroll and consistent with the GNOME guidelines.</li>
</ul>

<h3>Included software</h3>

<ul>
<li>Update <em>Tor Browser</em> to 8.0, based on <em>Firefox</em> 60 ESR.
<ul>
<li>
Based on <a href="https://blog.mozilla.org/blog/2017/11/14/introducing-firefox-quantum/" rel="nofollow"><em>Firefox Quantum</em></a>.
</li>
<li>
New Tor circuit view
</li>
</ul>
</li>
<li>
Update <em>Thunderbird</em> from 52 to <a href="https://www.thunderbird.net/en-US/thunderbird/60.0/releasenotes/" rel="nofollow">60</a>.
</li>
<li>
Update <em>Tor</em> to 0.3.4.7-rc.
</li>
<li>
Update <em>Electrum</em> to from 3.0.6 to <a href="https://github.com/spesmilo/electrum/blob/master/RELEASE-NOTES" rel="nofollow">3.1.3</a>.
</li>
</ul>

<h3>Hardware support</h3>

<p>The following updates make Tails work better on recent hardware (graphics, Wi-Fi, etc.):</p>

<ul>
<li>
Update <em>Linux</em> to 4.17 which also fixes the <a href="https://foreshadowattack.eu/" rel="nofollow">Foreshadow</a> attack.
</li>
<li>
Update the <em>DRM</em> and <em>Mesa</em> libraries to improve support for some graphics cards.
</li>
<li>
Update the Intel and AMD microcodes and most firmware packages.
</li>
</ul>

<h2>Fixed problems</h2>

<ul>
<li>
Stop displaying the <em>Enigmail</em> setup wizard every time Tails is restarted. (<a href="https://labs.riseup.net/code/issues/15693" rel="nofollow">#15693</a> and <a href="https://labs.riseup.net/code/issues/15746" rel="nofollow">#15746</a>)
</li>
<li>
Show a spinner while starting <em>Tor Browser</em>, <em>Tails documentation</em>, and <em>WhisperBack</em>. (<a href="https://labs.riseup.net/code/issues/15101" rel="nofollow">#15101</a>)
</li>
<li>
Use <em>Tor Browser</em> again for browsing the documentation offline. (<a href="https://labs.riseup.net/code/issues/15720" rel="nofollow">#15720</a>)
</li>
<li>
Show <em>Synaptic</em> and <em>Root Terminal</em> even when no administration password is set. (<a href="https://labs.riseup.net/code/issues/11013" rel="nofollow">#11013</a>)
</li>
<li>
<em>Tails Installer</em>
<ul>
<li>Link to upgrade documentation when upgrading. (<a href="https://labs.riseup.net/code/issues/7904" rel="nofollow">#7904</a>)</li>
<li>Hide the <strong>Reinstall</strong> option when the USB stick is too small. (<a href="https://labs.riseup.net/code/issues/14810" rel="nofollow">#14810</a>)</li>
<li>Correct the size of the USB stick in the confirmation dialog when reinstalling. (<a href="https://labs.riseup.net/code/issues/15590" rel="nofollow">#15590</a>)</li>
</ul>
</li>
</ul>

<p>For more details, read our <a href="https://git-tails.immerda.ch/tails/plain/debian/changelog" rel="nofollow">changelog</a>.<br />
<a rel="nofollow"></a></p>

<h1>Known issues</h1>

<ul>
<li>Starting Tails 3.9 from DVD is twice slower than earlier releases. (<a href="https://labs.riseup.net/code/issues/15915" rel="nofollow">#15915</a>)</li>
</ul>

<p>See the list of <a href="https://tails.boum.org/support/known_issues/" rel="nofollow">long-standing issues</a>.</p>

<h1>Get Tails 3.9</h1>

<ul>
<li>
To install, follow our <a href="https://tails.boum.org/install/" rel="nofollow">installation instructions</a>.
</li>
<li>
To upgrade, automatic upgrades are available from 3.7.1, 3.8, and 3.9~rc1 to 3.9.<br />
If you cannot do an automatic upgrade or if Tails fails to start after an automatic upgrade, please try to do a <a href="https://tails.boum.org/upgrade/" rel="nofollow">manual upgrade</a>.
</li>
<li>
<a href="https://tails.boum.org/install/download/" rel="nofollow">Download Tails 3.9.</a>
</li>
</ul>

<h1>What's coming up?</h1>

<p>Tails 3.10 is <a href="https://tails.boum.org/contribute/calendar/" rel="nofollow">scheduled</a> for October 23.<br />
Have a look at our <a href="https://tails.boum.org/contribute/roadmap" rel="nofollow">roadmap</a> to see where we are heading to.<br />
We need your help and there are many ways to <a href="https://tails.boum.org/contribute/" rel="nofollow">contribute to Tails</a> (<a href="https://tails.boum.org/donate?r=3.9" rel="nofollow">donating</a> is only one of them). Come <a href="https://tails.boum.org/about/contact/#tails-dev" rel="nofollow">talk to us</a>!</p>

<h1>Support and feedback</h1>

<p>For support and feedback, visit the <a href="https://tails.boum.org/support/" rel="nofollow">Support section</a> on the Tails website.</p>

